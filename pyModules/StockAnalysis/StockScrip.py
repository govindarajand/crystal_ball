#!/usr/bin/env python
# coding: utf-8
import pandas as pd
import os

class StockScrip:
    def __init__(self,ScripName,HistType):
        self.TechName = ScripName
        self.HistType = HistType
        self.stock_data = ""
    def UpdateData(self):
        "Update data here based on from and to date based on interval"
        self.stock_data = pd.read_csv(os.getcwd() + '/pyModules/' + 'SBIN.csv',parse_dates=['Date'])
        
    def CleanData(self):
        "Modify the class property when cleansing data"

