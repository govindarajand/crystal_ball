#!/usr/bin/env python
# coding: utf-8
import pandas as pd

try:
    from .StockScrip import StockScrip
except SystemError:
    from StockScrip import StockScrip

class StockAnalyser(StockScrip):
    def __init__(self,ScripName,HistType):
        super().__init__(ScripName,HistType)
        self.ScripName = ScripName
    def RSI(self,period=14):
        """ RSI = 100 - (100/(1+RS)) 
            First RS = (Average Gain)/(Average Loss)
            Average Gain = Total Gain/n 'Total Gain for the n period
            Average Loss = Total Loss/n 'Total Loss for the n period
            For following RS (For smoothing) = Numerator/Denominator
            Numerator = ([(Previous Average Gain) x (n-1) + Current Gain]/n)
            Denominator = ([(Previous Average Loss) x (n-1) + Current Loss]/n) """
        
        self.rsi_period = period
        
        #Calculating the change from previous day for each row
        self.stock_data["Change"] = self.stock_data["Close"].diff()
        
        #Gain and Loss are put in separate columns
        self.stock_data['Gain'] =  self.stock_data.apply(lambda row: row.Change if row.Change > 0 else 0,axis=1)
        self.stock_data['Loss'] =  self.stock_data.apply(lambda row: -1 * row.Change if row.Change < 0 else 0,axis=1)
        
        #Average gain and Avg Loss for the first row after the number of periods is calculated
        self.stock_data.loc[period,'AvgGain'] = self.stock_data[1:period].Gain.mean()
        self.stock_data.loc[period,'AvgLoss'] = self.stock_data[1:period].Loss.mean()
        
        #Store previous average gain and loss value of the row
        self.stock_data.value_gain_previous = self.stock_data.loc[period,'AvgGain']
        self.stock_data.value_loss_previous = self.stock_data.loc[period,'AvgLoss']
        
        #Average gain and Avg Loss for all the rows
        self.stock_data["AvgGain"] = self.stock_data.apply(lambda row: self.__rsi_avg_gain(row,period),axis = 1)
        self.stock_data["AvgLoss"] = self.stock_data.apply(lambda row: self.__rsi_avg_loss(row,period),axis = 1)
        
        #Calculate RS
        self.stock_data["RS"] = self.stock_data.apply(lambda row: row.AvgGain/row.AvgLoss, axis = 1)
        
        #Calculate RSI
        self.stock_data["RSI"] = self.stock_data.apply(lambda row: 100 if row.AvgLoss == 0 else (100 - (100/(1+row.RS))), axis = 1)
        
    def __rsi_avg_gain(self,row,period):
         if(row.name > period):
            row.AvgGain = ((self.stock_data.value_gain_previous * period - 1) + row.Gain)/period
            self.stock_data.value_gain_previous = row.AvgGain
         
         return row.AvgGain
    
    def __rsi_avg_loss(self,row,period):
         if(row.name > period):
            row.AvgLoss = ((self.stock_data.value_loss_previous * period - 1) + row.Loss)/period
            self.stock_data.value_loss_previous = row.AvgLoss
         
         return row.AvgLoss

    def getLastRSI(self):
        return print(self.stock_data.tail(1).RSI.to_string().split(" ")[-1])
