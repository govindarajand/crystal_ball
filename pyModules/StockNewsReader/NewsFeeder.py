#!/usr/bin/env python
# coding: utf-8

from bs4 import BeautifulSoup
import requests

livemint_search = requests.get("https://www.livemint.com/Search/Link/Keyword/SBI")
livemint_soup = BeautifulSoup(livemint_search.text,'html.parser')

livemint_links = set()
for search_result in livemint_soup.find_all('div',class_="mySearchView"):
    for link in search_result.find_all('a'):
        livemint_links.add(link.get('href'))

news_articles = []
for link in livemint_links:
    news_articles.append(requests.get("https://livemint.com/" + str(link)))

article_texts = []
content = ""
for news_article in news_articles:
    for article_content in BeautifulSoup(news_article.text,'html.parser').find_all('div',class_="mainArea"):
        for texts in article_content.find_all("p"):
            content = content + texts.text
    article_texts.append(content)
    content = ""

from nltk.tokenize import word_tokenize

tokenized_texts = []

for content in article_texts:
    tokenized_texts.append(word_tokenize(content))

from nltk.probability import FreqDist
from nltk.corpus import stopwords

stop_words=set(stopwords.words("english"))
better_tokenized_texts = []
filtered_words = []
stop_words.update([',','.'])
for text in tokenized_texts:
    for word in text:
        if word not in stop_words:
            filtered_words.append(word)
    better_tokenized_texts.append(filtered_words)
    filtered_words = []
