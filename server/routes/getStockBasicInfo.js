const express = require('express');
const getStockBasicInfo = express.Router({mergeParams:true});
var logger = require('../logger');

const mongoUtil = require('../mongoUtil');

getStockBasicInfo.get('/',(req,res,next)=> {
    const { spawn } = require('child_process');
    var py_get_basic_info = spawn('python3',['./pyModules/getBasicStockInfo.py',req.params.stockScrip],{ shell: true });
    py_get_basic_info.stdout.on('data',(data)=>{
        res.send(data.toString("utf-8"));
    });
    py_get_basic_info.stderr.on('data',(data)=>{
        res.send(data.toString("utf-8"));
    });
});
module.exports = getStockBasicInfo;
