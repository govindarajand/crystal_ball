var fs = require('fs');

writeToFile = (stringToWrite) => {
    fs.appendFile('server-log.txt', stringToWrite.toString(), (err) => {
        if(err)
            console.log("Error in logger function");
    });
}
var logger = {
    info: (info) => {
        writeToFile(Date()+ `: [INFO]: ${info} \n`);
    },
    warning: (warning) => {
        writeToFile(Date()+ `: [WARN]: ${warning} \n`);
    },
    error: (error) => {
        writeToFile(Date()+ `: [EROR]: ${error} \n`);
    }
};

module.exports = logger;