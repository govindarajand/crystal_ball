const express = require('express');

const mongoUtil = require('./mongoUtil');
const logger = require('./logger');

//Import routes
const getStockBasicInfoRouter = require('./routes/getStockBasicInfo');

const app = express();
const PORT = process.env.PORT || 5000;


// mongoUtil.connectToMongoServer((error) => {
    app.get('/', (req,res,next) =>{
        res.sendFile('/html_files/root.html',{root:__dirname});

    });

    app.use('/getBasicInfo/:stockScrip',getStockBasicInfoRouter);
// });

app.listen(PORT, () => {
    logger.info("Server started" + Date.now());
});